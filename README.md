## 一、数组

### `multArray`二维数组转换

将数组（array）拆分成多个子数组，并将这些子数组组成一个新数组。

```js
multArray(array, count)
```

**参数**

-   `array`需要处理的数组
-   `count = 8`子数组需要的长度

**示例**

```js
multArray([1, 2, 3, 4, 5, 6], 2)
=> [[1, 2], [3, 4], [5, 6]]

multArray(['a', 'b', 'c', 'd'], 3)
=> [['a', 'b', 'c'], ['d']]
```

**源码**

```js
const multArray = (arr, count = 2) => {
  let pages = []
  arr.forEach((item, index) => {
    const page = Math.floor(index / count);
    (!pages[page]) && (pages[page] = []);
    pages[page].push(item);
  })
};
```



### `flatten`扁平化数组

将多层嵌套数组（array）拆分成一个数组

```js
flatten(array)
```

**参数**

-   `array`多层嵌套数组

**示例**

```js
flatten([1, [2], [3], [4, 5]])

// [1, 2, 3, 4, 5]
```

**源码**

```js
//扁平化Map方法
const flatten = arr => [].concat(...arr.map(item 今(Array.isArray(item) ? flatten(item) : item)));
//扁平化 reduce方法
const flatten = arr => arr.reduce((a, i) = a.concat(Array.isArray(i) ? flatten(i) : i), []);
```



### `flattenDeep`指定层级扁平化数组

将多层嵌套数组（array）拆分成指定层级数组

```js
flattenDeep(array, depth)
```

**参数**

-   `array`多层嵌套数组 `depth = ` 减少的嵌套层级数

**示例**

```js
flattenDeep(['a', ['b', ['c', ['d']], 'e']], 1)
// => ['a', 'b', ['c', ['d']], 'e']

// ES6方法 `flat(depth)`
['a', ['b', ['c', ['d']], 'e']].flat(1)
// => ['a', 'b', ['c', ['d']], 'e']
```

**源码**

```js
const flattenDeep = (arr, depth = 1) => arr.reduce((a, i) => a.concat(depth > 1 && Array.isArray(i) ? flatten(i, depth - 1) : i), [])
```

### `isArrayEqual`检查两个数组各项相等

比较两个数组内的各项值是否相等，返回一个`Boolean`值

```js
isArrayEqual(arrOne, arrTwo)
```

**参数**

-   `arrOne` 要检查的数组
-   `arrTwo` 要检查的数组

**示例**

```js
isArrayEqual([6, 5, 2, 4, 1, 3], [1, 2, 3, 4, 5, 6])
// => true

isArrayEqual([6, 5, 2, 7, 1, 3], [1, 2, 3, 4, 5, 6])
// => false
```

**源码**

```js
const isArrayEqual = (a, b, res = true) => {
  if (a.length !== b.length) return (res = false);
  const s = new Set(b);
  if (a.find(x => !s.has(x))) return (res = false);
  return res;
}
```



### `allEqual`检查数组各项相等

```js
allEqual(array)
```

**参数**

-   `array` 要检查的数组

**示例**

```js
allEqual([1, 2, 3, 4, 5, 6])
// => false

allEqual([1, 1, 1, 1])
// => true
```

**源码**

```js
const allEqual = arr => arr.every(item => item === arr[0]);
```



### `diffArray`具有唯一`array`值的数组

创建一个具有唯一 array 值的数组，每个值不包含在其他给定的数组中

```js
diffArray(arrOne, arrTwo)
```

**参数**

-   `arrOne` 要检查的数组
-   `arrTwo`要排除的数组

**示例**

```js
diffArray(['a', 2, 6, 7], ['a', 2, 9, 'b'])
// => [ 6, 7 ]
```

**源码**

```js
const diffArray = (a, b) => {
  const s = new Set(b);
  let arr = a.filter(x => !s.has(x));
  return arr;
}
```



### `haveArr`具有共同`array`值的数组

创建一个具有共同 array 值的数组，每个值包含在其他给定的数组中

```js
haveArr(arrOne, arrTwo)
```

**参数**

-   `arrOne` 要检查的数组
-   `arrTwo`要包含的数组

**示例**

```js
haveArr([1, 2, 6, 7], [1, 2, 9, 5])
// => [ 1, 2 ]
```

**源码**

```js
const haveArr = (a, b) => {
  const s = new Set(b);
  return a.filter(x => s.has(x));
};

// ES6 includes
const haveArr = (arr, values) => arr.filter(v => values.includes(v));
```



### `uniqueArray`数组去重

创建一个去重后的 array 数组副本

```js
uniqueArray(array)
```

**参数**

-   `array` 要去重的数组

**示例**

```js
uniqueArray([1, 2, 2, 3, 4, 4, 5])
// => [ 1, 2, 3, 4, 5 ]
```

**源码**

```js
// 第一种
const uniqueArray = (...arr) => [...new Set(arr)];

// 第二种
const uniqueArray  = (...arr) => Array.from(new Set(arr));
```



### `uniqueArrayObject`数组对象去重

创建一个去重后的 array 数组对象副本

```js
uniqueArrayObject(array)
```

**参数**

-   `array` 要去重的数组
-   `key` 要去重的对象属性值

**示例**

```js
const responseList = [
    { id: 1, a: 1 },
    { id: 2, a: 2 },
    { id: 3, a: 3 },
    { id: 1, a: 4 },
    { id: 2, a: 2 },
    { id: 3, a: 3 },
    { id: 1, a: 4 },
    { id: 2, a: 2 },
    { id: 3, a: 3 },
    { id: 1, a: 4 },
    { id: 2, a: 2 },
    { id: 3, a: 3 },
    { id: 1, a: 4 },
]

uniqueArrayObject(responseList, 'id')

// => [ { id: 1, a: 1 }, { id: 2, a: 2 }, { id: 3, a: 3 } ]
```

**源码**

```js
const uniqueArrayObject = (arr, key) => {
  return arr.reduce((acc, cur) => {
    const ids = acc.map(item => item[key]);
    return ids.includes(cur[key]) ? acc : [...acc, cur];
  }, []);
}
```



### `treeData`生成树结构数据

该函数传入一个数组， 每项`id`对应其父级数据`parent_id`，返回一个树结构数组

```js
treeData(array, id, parent_id)
```

**参数**

-   `array` 要生成树结构的数组
-   `id` 自定义属性名
-   `parent_id` 父级自定义属性名

**示例**

```js
const comments = [
    { id: 1, parent_id: null },
    { id: 2, parent_id: 1 },
    { id: 3, parent_id: 1 },
    { id: 4, parent_id: 2 },
    { id: 5, parent_id: 4 },
]

treeData(comments)

// => [ { id: 1, parent_id: null, children: [ { id: 2, parent_id: 1, children: [ { id: 4, parent_id: 2, children: [ { id: 5, parent_id: 4, children: [] } ] } ] }, { id: 3, parent_id: 1, children: [] } ] } ]
```

**源码**

```js
const treeData = (arr, id = null, link = 'parent_id') => arr.filter(item => item[link] === id).map(item => ({...item, children: treeData(arr, item.id) }))
```



### `flatArrFunc`树结构数据生成数组

该函数传入一个树结构数组，返回一个数组

```js
flatArrFunc(array)
```

**参数**

-   `array` 要遍历树结构的数组

**示例**

```js
const list = [ { id: 1, parent_id: null, children: [ { id: 2, parent_id: 1, children: [ { id: 4, parent_id: 2, children: [ { id: 5, parent_id: 4, children: [] } ] } ] }, { id: 3, parent_id: 1, children: [] } ] } ]

flatArrFunc(list)

// => [
    { id: 1, parent_id: null },
    { id: 2, parent_id: 1 },
    { id: 3, parent_id: 1 },
    { id: 4, parent_id: 2 },
    { id: 5, parent_id: 4 },
]
```

**源码**

```js
const flatArrFunc = (list) => {
  let array = [];
  for (const item of list) {
    const json = { ...item };
    if (item.children) {
      delete json.children;
    }
    
    array.push(json);
    if (item.children && item.children.length) {
      array = [].concat(array, flatArrFunc(item.children));
    }
  }
  return array;
}
```



### `ascArr`数组升序

返回升序后的新数组

> sort()方法会改变原数组，默认按 unicode 码顺序排列

```js
ascArr(array)
```

**参数**

-   `array` 要检查的排序数组

**示例**

```js
ascArr([3, 2, 3, 4, 1])
// => [ 1, 2, 3, 3, 4 ]
```

**源码**

```js
// 通过ES6 ...展开运算符浅拷贝一份新数组
const ascArr = arr => [...arr].sort((a, b) => a - b);
```



### `descArr`数组降序

返回降序后的新数组

```js
descArr(array)
```

**参数**

-   `array` 要检查的排序数组

**示例**

```js
descArr([3, 2, 3, 4, 1])
// => [ 1, 2, 3, 3, 4 ]
```

**源码**

```js
const descArr = arr => [...arr].sort((a, b) => b - a);
```



### `shuffle`随机排序

创建一个随机的数组，使用`Fisher-Yates`算法随机排序数组的元素

```js
shuffle(array)
```

**参数**

-   `array` 要随机的数组

**示例**

```js
shuffle([2, 3, 1])
// => [3, 1, 2]
```

**源码**

```js
const shuffle = ([...arr]) => {
  let m = arr.length;
  while (m) {
    const i = Math.floor(Math.random() * m--);
    [arr[m], arr[i]] = [arr[i], arr[m]];
  };
  return arr;
}
```



### `takeArray`截取数组开始指定的元素

从 array 数组的最开始一个元素开始提取 n 个元素

```js
takeArray(array, n)
```

**参数**

-   `array`要检索的数组。
-   `n=`要提取的元素`n`个数。

**示例**

```js
takeArray([2, 3, 1], 2)
// => [2, 3]
```

**源码**

```js
const takeArray = (arr, n = 1) => arr.slice(0, n);
```



### `takeLastArray`截取数组最后指定的元素

从 array 数组的最后一个元素开始提取 n 个元素

```js
takeLastArray(array, n)
```

**参数**

-   `array`要检索的数组。
-   `n=`要提取的元素`n`个数。

**示例**

```js
takeLastArray([2, 3, 1], 2)
// => [3, 1]
```

**源码**

```js
const takeLastArray = (arr, n = 1) => arr.slice(0, -n);
```



### `cloneArray`克隆数组

浅拷贝一份数组副本

```js
cloneArray(array)
```

**参数**

-   `array`要复制的数组

**示例**

```js
cloneArray([1, 24])
// => [1, 24]
```

**源码**

```js
// ES6 ...
const cloneArray = arr => [...arr];

// ES6 Array.from
const cloneArray = arr => Array.form(arr);

// concat()
const cloneArray = arr => [].concat(arr);

// map
const cloneArray = arr => arr.map(x => x);
```



### `maxArray`数组中最大值

过滤原数组中所有的非假值元素，返回数组中的最大值

```js
maxArray(array)
```

**参数**

-   `array`待处理的数组

**示例**

```js
maxArray([0, -1, -2, -3, false])
// => 0
```

**源码**

```js
const maxArray = arr => Math.max(...arr.filter(v => Boolean(v) || v === 0));
```



### `minArray`数组中最小值

过滤原数组中所有的非假值元素，返回数组中的最小值

```js
minArray(array)
```

**参数**

-   `array`待处理的数组

**示例**

```js
minArray([0, -1, -2, -3, false])
// => -3
```

**源码**

```js
const minArray = arr => Math.min(...arr.filter(v => Boolean(v) || v === 0));
```



### `validArray`去除数组中的无效值

创建一个新数组，包含原数组中所有的非假值元素。例如`false`, `null`,`0`, `""`, `undefined`, 和 `NaN` 都是被认为是“假值”。

```js
validArray(array)
```

**参数**

-   `array`待处理的数组

**示例**

```js
minArray([0, 1, false, 2, '', 3])
// => [1, 2, 3]
```

**源码**

```js
const validArray = arr => arr.filter(Boolean);
```



### `filterParams`去除请求参数中的无效值

创建一个新的请求参数，包含原请求参数中所有的无效值元素。例如, `null`, `""`, `undefined`, 和 `NaN` 都是被认为是“无效值”。

```js
filterParams(obj)
```

**参数**

-   `obj`待处理的请求参数

**示例**

```js
const params = {
    a:1,
    b: '2',
    c: null,
    d: '',
    e: undefined,
    f: NaN,
}
filterParams(params)
// => {a: 1, b: '2'}
```

**源码**

```js
const filterParams = (obj) => {
  let _newPar = {};
  for (let key in obj) {
    if((obj[key] === 0 || obj[key] === false || obj[key]) && obj[key].toString().replace(/(^\s*)|(\s*$/g, '') !== '') {
       _newPar[key] = obj[key];
       }
  }
  return _newPar;
};
```



## 二、对象

### `cloneObject`克隆对象

浅拷贝一份对象副本

```js
cloneObject(object)
```

**参数**

-   `object`要复制的对象

**示例**

```js
const a = { x: 1, y: 1 }
const b = cloneObject(a)
// => a !== b
```

**源码**

```js
// ES6
const cloneObject = (obj, temp = {}) => (temp = { ...obj });

// Object.assign()
const cloneObject = obj => Object.assign({}, obj);
```



## 三、函数

### `debounce`函数防抖

在事件被触发 n 秒后再执行回调，如果在这 n 秒内又被触发，则重新计时。

```js
debounce(fn, wait)
```

**参数**

-   `fn` 要防抖动的函数
-   `wait=500`需要延迟的毫秒数

**示例**

```
debounce(()=> { console.log('debounce') }, 1000)
// => 1秒后打印'debounce'
```

**源码**

```js
const debounce = (() => {
  let timer = {};
  return function (func, wait = 500) {
    let context = this; // 注意 this 指向
    let args = arguments; // arguments中存着e
    let name = arguments[0].name || 'arrow'; // 箭头函数
    if(timer[name]) clearTimeout(timer[name]);
    timer[name] = setTimeout(() => {
      func.apply(this, arrgs);
    }, wait)
  }
})()
```



### `throttle`函数节流

规定一个单位时间，在这个单位时间内，只能有一次触发事件的回调函数执行，如果在同一个单位时间内某事件被触发多次，只有一次能生效。

```js
throttle(fn, wait)
```

**参数**

-   `fn` 要节流的函数
-   `wait=500`需要延迟的毫秒数

**示例**

```js
throttle(() => {
    console.log('throttle')
}, 1000)
// 1秒多次触发打印一次`throttle`
```

**源码**

```js
const throttle = (() => {
  let timeout = null;
  return function (func, wait = 500) {
    let context = this;
    let args = arguments;
    if(!timeout) {
      timeout = setTimeout(() => {
        timeout = null;
        func.apply(context, args);
      }, wait);
    };
  };
})()
```



### `typeFn`类型判断

判断是否是 `Array` `Object` `String` `Number`类型

```js
typeFn.type(value)
```

**参数**

-   `type` 数据类型
-   `value`要检验的值

**示例**

```js
typeFn.String('1')
typeFn.Number(1)
typeFn.Boolean(false)
typeFn.Null(null)
typeFn.Array([1, 2])
typeFn.Object({ a: 1 })
typeFn.Function(() => {})

// => true
```

**源码**

```js
let typeFn = {};
caonst curring = (fn, arr = []) => {
  let len = fn.lengthl
  return (...args) => {
    arr = arr.concat(args);
    if(arr.length < len) {
      return curring(fn, arr);
    };
    return fn(...arr);
  };
};
const isType = (type, content) => Object.prototype.toString.call(content) === `[object] ${type}]`;
['String', 'Number', 'Boolean', 'Null', 'Array', 'Object', 'Function'].forEach(item => {
  typeFn[item] = curring(isType)(item);
});
```



### `calcFn`加减乘除运算

因为 JavaScript 遵循 IEEE 754 数学标准，使用 64 位浮点数进行运算。在进行十进制运算时会导致精度丢失。

```js
calcFn.add(value1, value2, value3)
```

**参数**

-   `add`、`sub`、`mul`、`div`运算符
-   `value`要计算的值

**示例**

```js
解决 0.1+0.2 !== 0.3 问题
//加法
calcFn.add(0.1, 0.2) // 0.3

//减法
calcFn.sub(0.1, 0.2) // 0.1

//乘法
calcFn.mul(0.2, 0.3) // 0.06

// 乘法
calcFn.div(0.1, 0.2) // 0.5
```

**源码**

```js
const calcFn = {
  add() {
    let arg = Array.from(arguments);
    return arg.reduce((total,num) => accAdd(total, num));
  },
  sub() {
    let arg = Array.from(arguments);
    return arg.reduce((total,num) => accAdd(total, -num));
  },
  mul() {
    let arg = Array.from(arguments);
    return arg.reduce((total,num) => accMul(total, num));
  },
  div() {
    let arg = Array.from(arguments);
    return arg.reduce((total,num) => accDiv(total, num));
  }
};

const accAdd = (arg1, arg2) => {
  let r1, r2, m;
  try {
    r1 = arg1.toString().split('.')[1].length;
  } catch (e) {
    r1 = 0
  }
  try {
    r2 = arg2.toString()/split('.')[1].length;
  } catch (e) {
    r2 = 0
  };
  m = Math.pow(10, Math.max(r1,r2));
  return (arg1 * m + arg2 * m) / m;
};

const accMul = (arg1, arg2) => {
  let m = 0;
  let s1 = arg1.toString();
  let s2 = arg2.toString();
  try {
    m += s1.split('.')[1].length
  } catch (e){}
  try {
    m += s2.split('.')[1].length
  } catch (e){}
  return Number(s1.replace('.', '')) * Number(s2.replace('.', '')) / Math.pow(10,m);
}

const accDiv = (arg1, arg2) => {
  let t1 = 0,t2 = 0,r1,r2;
  try {
    t1 = arg1.toString().split('.')[1].length;
  } catch (e) {}
  try {
    t2 = arg2.toString().split('.')[1].length;
  } catch (e) {}
  r1 = Number(arg1.toString().replace('.', ''));
  r2 = Number(arg2.toString().replace('.', ''));
  return (r1 / r2) * Math.pow(10, t2 - t1);
};
```



## 四、字符串

### `isNil`值是否是`null`或`undefined`

```js
isNil(value)
```

**参数**

-   `value` 要检验的值

**示例**

```js
isNil(null)
isNil(undefined)
// => true
```

**源码**

```js
const isNil = val => val === undefined || val === null;
```



## 五、数字

### `thousands`数字每隔三位数加分号

```js
thousands(number)
```

**参数**

-   `number` 数字或者浮点数

**示例**

```js
thousands(12255552323)
// => 12,255,552,323
thousands(592535.2641)
// => 592,535.2641
```

**源码**

```js
const thousands = num => num.toString().replace(num.toString().indexOf('.') > -1 ? /(\d)(?=(\d{3})+\.)/g : /(\d)(?=(\d{3})+$)/g, '$1,')
```



### `numPercentage`将小数转化为百分数

```js
numPercentage(number,digit)
```

**参数**

-   `number` 小数
-   `digit` 保留位数

**示例**

```js
numPercentage(12.655566, 3)
// => 1265.557%
```

**源码**

```js
const numPercentage = (number, digit = 2) => {
  let res = '';
  res = (Math.abs(Number(number) * 100)).toFixed(digit) + '%';
  return res
}
```



### `randomNumber`指定范围的随机整数

```js
randomNumber(min, max)
```

**参数**

-   `min` 指定范围最小值
-   `max` 指定范围最大值

**示例**

```js
randomNumber(0, 10)
// => 7
// => 2
```

**源码**

```js
const randomNumber = (min = 0, max = 10) => Math.floor(Math.random() * (max - min +1)) + min;
```



### `average`求平均值

```js
average(value1, value2, value3)
```

**参数**

-   `value` 数字

**示例**

```js
average(...[1, 2, 3])
average(1, 2, 3)
// => 2
```

**源码**

```js
const average = (...nums) => nums.reduce((acc, val) => acc+ val, 0) / nums.length;
```



### `averageBy`求数组对象内某对象的平均值

```
averageBy(array, fn)
```

**参数**

-   `array` 要迭代的数组
-   `fn` 迭代函数

**示例**

```js
averageBy([{ n: 4 }, { n: 2 }, { n: 8 }, { n: 6 }], o => o.n)
averageBy([{ n: 4 }, { n: 2 }, { n: 8 }, { n: 6 }], 'n')
// => 5
```

**源码**

```js
const averageBy = (arr, fn) => arr.map(typeof fn === 'function' ? fn : val => val[fn]).reduce((acc, val) => acc + val, 0) / arr.length;
```



### `aboutEqual`两个值是否约等于

传入两个数字是否大致相等，误差在可接受范围内

```js
aboutEqual(n1, n2, epsilon)
```

**参数**

-   `n1 n2` 要比较的数字
-   `epsilon` 误差可接受范围内

**示例**

```js
aboutEqual(25, 2, 0.06)
// => false
aboutEqual(2.06, 2, 0.06)
// => false
aboutEqual(2.01, 2, 0.06)
// => true
```

**源码**

```js
const aboutEqual = (n1, n2, epsilon = 0.001) => Math.abs(n1 - n2) < epsilon;
```



### `getLineSize`计算两点之间的距离

勾股定理计算两点之间的距离

```js
getLineSize = (x1, y1, x2, y2)
```

**参数**

-   `x1 y1 x2 y2`坐标点

**示例**

```js
getLineSize(0, 0, 3, 4)
// => 5
```

**源码**

```js
const getLineSize = (x1, y1, x2, y2) => Math.hypot(x2 - x1, y2 - y1);
```



### `sum`数组中值总和

```js
sum(value1, value2, value3)
```

**参数**

-   `value1 value2 value3`要迭代的数字

**示例**

```js
sum(1, 2, 3, 4)
sum(...[1, 2, 3, 4])
// => 10
```

**源码**

```js
const sum = (...arr) => [...arr].reduce((acc, val) => acc + val, 0);
```



## 六、浏览器

### `copyText`H5复制文本

```
copyText(content, callback)
```

**参数**

-   `content`要复制文字
-   `callback` 回调用户提示

**示例**

```js
copyText(content, text => {
    this.$toast(text)
})
```

**源码**

```js
const copyText = (content, callback) => {
  if (!document.queryCommandSupported('copy')) {
    // 为了兼容有些浏览器 queryCommandSupported 的判断
    console.log('浏览器不支持');
    return;
  };
  let textarea = document.createElement('textarea');
  textarea.value = content;
  textarea.readOnly = 'readOnly';
  document.body.appendChild(textarea);
  textarea.select(); // 选择对象
  textarea.setSelectionRange(0, content.length); // 核心
  let result = document.execCommand('copy'); // 执行浏览器复制命令
  callback && callback(result ? '复制成功' : '复制失败');
  textarea.remove();
}
```



### `getCurrentURL`获取当前的 URL 地址

该函数返回当前页面的 URL 地址。

**示例**

```js
getCurrentURL()
// => "https://juejin.cn"
```

**源码**

```js
const getCurrentURL = () => window.location.href;
```




### `scrollToTop`返回顶部

平滑地滚动到当前页面的顶部。

**示例**

```js
scrollToTop()
// => 当前页面的顶部
```

**源码**

```js
const scrollToTop = () => {
  const c = document.documentElement.scrollTop || document.body.scrollTop;
  if (c > 0) {
    window.requestAnimationFrame(scrollToTop);
    window.scrollTo(0, c - c / 8);
  };
};
```



### `smoothScroll`平滑滚动页面

平滑滚动到浏览器窗口的可见区域

**示例**

```js
smoothScroll('#fooBar');
// => 平滑滚动到ID为fooBar的元素
smoothScroll ('.fooBar' );
// => 使用fooBar类平滑滚动到第一个元素
```

**源码**

```js
const smoothScroll = element => document.querySelector(element).scrollIntoView({ behavior: 'smooth' });
```



### 5.`isCurrentPage`是否是当前页面

浏览器的选项卡是否是用户在浏览

**示例**

```js
isCurrentPage()
// => true
```

**源码**

```js
isCurrentPage = () => !document.hidden;
```



## 七.环境

### 1.`isBrowser`是否是浏览器

返回当前运行时环境是否为浏览器

**示例**

```js
isBrowser()
// => true (browser)
// => false (Node)
```

**源码**

```js
const isBrowser = () => ![typeof window, typeof document].includes('undefined');
```



### 2.`isWechatBrowser`判断微信浏览器还是普通h5

**示例**

```js
isWechatBrowser()
// => true
```

**源码**

```js
const isWechatBrowser = (() => {
  let ua = navigator.userAgent.toLowerCase();
  return /micromessenger/.test(ua);
})()
```



### 3.`isMobile`判断是否是移动端

**示例**

```js
isMobile()
// => true
```

**源码**

```js
const isMobile = () => /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|OperaMini/i.test(navigator.userAgent);
```



## 八、其他
### `after`

此方法创建一个函数，当他被调用`n`或更多次之后将马上触发`func`

```js
after(n, func)
```

**参数**

-   `n`(number): `func` 方法应该在调用多少次后才执行。
-   `func`(Function): 用来限定的函数。

**返回**

(Function): 返回新的限定函数。

**示例**

```js
 const saves = ['profile', 'settings']
 const done = after(saves.length, () => console.log('done saving!'))
 
 forEach(saves, type => asyncSave({ 'type': type, 'complete': done }))
 // => 打印 'done saving!' 在两个异步保存完成后。
```

**源码**

```js
const after = (n, func) =>{
  if (typeof func !== 'function') {
    throw new TypeError('Expected a function');
  };
  n = n || 0;
  return (...args) => {
    if (--n < 1) {
      return func.apply(this, args);
    };
  };
};
```

### `before`

创建一个调用`func`的函数，通过`this`绑定和创建函数的参数调用`func`，调用次数不超过 `n` 次。 之后再调用这个函数，将返回一次最后调用`func`的结果。

```js
before(n, func)
```

**参数**

-   `n`(number): 超过多少次不再调用`func`（注：限制调用`func`的次数）。
-   `func`(Function): 限制执行的函数。

**返回**

(Function): 返回新的限定函数。

**示例**

```js
 jQuery(element).on('click', _.before(5, addContactToList));
// => 最多只允许点击4次
```

**源码**

```js
const before = (n, func) => {
  let result;
  if (typeof func !== 'function') {
    throw new TypeError('Expected a function');
  };
  return function (...args) {
    if (--n > 0) {
      result = func.apply(this, args);
    };
    if (n <= 1) {
      func = undefined;
    };
    return result;
  };
};
```

### `castArray`

创建一个调用`func`的函数，通过`this`绑定和创建函数的参数调用`func`，调用次数不超过 `n` 次。 之后再调用这个函数，将返回一次最后调用`func`的结果。

```js
castArray(value)
```

**参数**

-   `value`(*): 要处理的值。

**返回**

(Array): 返回转换后的数组。

**示例**

```js
castArray(1);
// => [1]
 
castArray({ 'a': 1 });
// => [{ 'a': 1 }]
 
castArray('abc');
// => ['abc']
 
castArray(null);
// => [null]
 
castArray(undefined);
// => [undefined]
 
castArray();
// => []
 
const array = [1, 2, 3];
console.log(castArray(array) === array);
// => true
```

**源码**

```js
const castArray = (...args) => {
  if (!args.length) {
    return [];
  };
  const value = args[0];
  return Array.isArray(value) ? value : [value];
};
```

### `clamp`

返回限制在 `lower` 和 `upper` 之间的值。

```js
clamp(number, [lower], upper)
```

**参数**

-   `number` (number): 被限制的值。
-   `[lower]` (number): 下限。
-   `upper` (number): 上限。

**返回**

(number): 返回被限制的值。

**示例**

```js
clamp(-10, -5, 5);
// => -5
 
clamp(10, -5, 5);
// => 5
```

**源码**

```js
const clamp = (number, lower, upper) => {
  number = +number;
  lower = +lower;
  upper = +upper;
  lower = lower === lower ? lower : 0;
  upper = upper === upper ? upper : 0;
  if (number === number) {
    number = number <= upper ? number : upper;
    number = number >= lower ? number : lower;
  };
  return number;
};
```

### `compact`

创建一个新数组，包含原数组中所有的非假值元素。例如`false`, `null`,`0`, `""`,`undefined`, 和 `NaN` 都是被认为是“假值”。

```js
compact(array)
```

**参数**

-   `array` (Array): 待处理的数组

**返回**

(Array): 返回过滤掉假值的新数组。

**示例**

```js
compact([0, 1, false, 2, '', 3]);
// => [1, 2, 3]
```

**源码**

```js
const compact = (array) => {
  let resIndex = 0;
  const result = [];

  if (array == null) {
    return result;
  };

  for (const value of array) {
    if (value) {
      result[resIndex++] = value;
    };
  };
  return result;
};
```

### `create`

创建一个继承 `prototype` 的对象。 如果提供了 `prototype`，它的可枚举属性会被分配到创建的对象上。

```js
create(prototype, [properties])
```

**参数**

-   `prototype` (Object): 要继承的对象。
-   `[properties]` (Object): 待分配的属性。

**返回**

(Object): 返回新对象。

**示例**

```js
function Shape() {
  this.x = 0;
  this.y = 0;
}
 
function Circle() {
  Shape.call(this);
}
 
Circle.prototype = create(Shape.prototype, {
  'constructor': Circle
});
 
let circle = new Circle;
circle instanceof Circle;
// => true
 
circle instanceof Shape;
// => true
```

**源码**

```js
const create = (prototype, properties) => {
  prototype = prototype === null ? null : Object(prototype);
  const result = Object.create(prototype);
  return properties == null ? result : Object.assign(result, properties);
};
```

### `defaultTo`

检查`value`，以确定一个默认值是否应被返回。如果`value`为`NaN`, `null`, 或者 `undefined`，那么返回`defaultValue`默认值。

```js
defaultTo(value, defaultValue)
```

**参数**

-   `value` (*): 要检查的值。
-   `defaultValue` (*): 默认值。

**返回**

(*): 返回 resolved 值。

**示例**

```js
defaultTo(1, 10);
// => 1
 
defaultTo(undefined, 10);
// => 10
```

**源码**

```js
const defaultTo = (value, defaultValue) => {
  return (value == null || value !== value) ? defaultValue : value;
};
```

### `defer`

推迟调用`func`，直到当前堆栈清理完毕。 调用时，任何附加的参数会传给`func`。

```js
defer(func, [args])
```

**参数**

-   `func` (Function): 要延迟的函数。
-   `[args]` (...*): 会在调用时传给 func 的参数。

**返回**

(number):返回计时器 id。

**示例**

```js
defer(function(text) {
  console.log(text);
}, 'deferred');
// => 一毫秒或更久一些输出 'deferred'。
```

**源码**

```js
const defer = (func, ...args) => {
  if (typeof func !== 'function') {
    throw new TypeError('Expected a function');
  };
  return setTimeout(func, 1, ...args);
};
```

### `delay`

延迟 `wait` 毫秒后调用 `func`。 调用时，任何附加的参数会传给`func`。

```js
delay(func, wait, [args])
```

**参数**

-   `func` (Function): 要延迟的函数。
-   `wait` (number): 要延迟的毫秒数。
-   `[args]` (...*): 会在调用时传入到 `func` 的参数。

**返回**

(number): 返回计时器 id

**示例**

```js
delay(function(text) {
  console.log(text);
}, 1000, 'later');
// => 一秒后输出 'later'。
```

**源码**

```js
const delay = (func, wait, ...args) => {
  if (typeof func !== 'function') {
    throw new TypeError('Expected a function');
  };
  return setTimeout(func, +wait || 0, ...args);
};
```

### `endsWith`

检查字符串`string`是否以给定的`target`字符串结尾。

```js
endsWith([string=''], [target], [position=string.length])
```

**参数**

-   `[string='']` (string): 要检索的字符串。
-   `[target]` (string): 要检索字符。
-   `[position=string.length]` (number): 检索的位置。

**返回**

(boolean): 如果字符串`string`以`target`字符串结尾，那么返回 `true`，否则返回 `false`。

**示例**

```js
endsWith('abc', 'c');
// => true
 
endsWith('abc', 'b');
// => false
 
endsWith('abc', 'b', 2);
// => true
```

**源码**

```js
const endsWith = (string, target, position) => {
  const { length } = string;
  position = position === undefined ? length : +position;
  if (position < 0 || position != position) {
    position = 0;
  } else if (position > length) {
    position = length;
  };
  const end = position;
  position -= target.length;
  return position >= 0 && string.slice(position, end) == target;
};
```

### `eq`

执行`SameValueZero` 比较两者的值，来确定它们是否相等。

```js
eq(value, other)
```

**参数**

-   `value` (*): 要比较的值。
-   `other` (*): 另一个要比较的值。

**返回**

(boolean): 如果两个值相等返回 `true` ，否则返回 `false` 。

**示例**

```js
let object = { 'a': 1 };
let other = { 'a': 1 };
 
eq(object, object);
// => true
 
eq(object, other);
// => false
 
eq('a', 'a');
// => true
 
eq('a', Object('a'));
// => false
 
eq(NaN, NaN);
// => true
```

**源码**

```js
const eq = (value, other) => {
  return value === other || (value !== value && other !== other);
};
```
